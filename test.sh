#!/usr/bin/env bash
# run the test case
# loop so we can re-generate the token
for i in {1..2}; do
    TOKEN=$(curl -s http://127.0.0.1:9090/v1/auth/token -u medium:medium | awk '{print $2}')
    for test in {1..2}; do
        siege -b -r 1 -c 10 --header="Authorization: Bearer $TOKEN" http://127.0.0.1:9090/v1/book/1449311601
        echo "TOKEN: $TOKEN"
    done
done